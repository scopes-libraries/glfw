using import enum
using import utils
using import utils.va
using import utils.bindings

load-library "/usr/lib/libglfw.so.3"

spice include+ (code)
    sc_import_c ".c"
        code as string
        list;
        Scope;

run-stage;

inline (native...)
    inline def (arg name)
        static-if (none? (va-pos arg native...)) ""
        else
            "#define GLFW_EXPOSE_NATIVE_" .. name .. """"

    vvv bind glfw
    vvv include+
    ..
        """"#include <GLFW/glfw3.h>
        def 'win32 "WIN32"
        def 'cocoa "COCOA"
        def 'x11 "X11"
        def 'wayland "WAYLAND"
        def 'wgl "WGL"
        def 'nsgl "NSGL"
        def 'gls "GLX"
        def 'egl "EGL"
        def 'omesa "OSMESA"
        """"#include <GLFW/glfw3native.h>

    define glfw
        ..
            'remove-prefix glfw.extern "glfw"
            'remove-prefix glfw.typedef "glfw"
            'remove-prefix glfw.define "glfw"

    define callbacks
        unlet Error
        enum Error plain
            Init
            Window

        inline setErrorCallback (window callback)
            glfw.setErrorCallback window
                static-typify callback i32 rawstring

        inline setMonitorCallback (window callback)
            glfw.setMonitorCallback window
                static-typify callback
                    mutable pointer glfw.monitor i32

        inline setWindowPosCallback (window callback)
            glfw.setWindowPosCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ i32 i32

        inline setWindowSizeCallback (window callback)
            glfw.setWindowSizeCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ i32 i32

        inline setWindowCloseCallback (window callback)
            glfw.setWindowCloseCallback window
                static-typify callback
                    mutable pointer glfw.window

        inline setWindowRefreshCallback (window callback)
            glfw.setWindowRefreshCallback window
                static-typify callback
                    mutable pointer glfw.window

        inline setWindowFocusCallback (window callback)
            glfw.setWindowFocusCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ i32

        inline setWindowIconifyCallback (window callback)
            glfw.setWindowIconifyCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ i32

        inline setWindowSetFramebufferSizeCallback (window callback)
            glfw.setWindowSetFramebufferSizeCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ i32 i32

        inline setKeyCallback (window callback)
            glfw.setKeyCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ i32 i32 i32 i32

        inline setCharCallback (window callback)
            glfw.setCharCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ u32

        inline setCharModsCallback (window callback)
            glfw.setCharModsCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ u32 i32

        inline setMouseButtonCallback (window callback)
            glfw.setMouseButtonCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ i32 i32 i32

        inline setCurserPosCallback (window callback)
            glfw.setCursorPosCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ f64 f64

        inline setCurserEnterCallback (window callback)
            glfw.setCursorPosCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ i32

        inline setDropCallback (window callback)
            glfw.setDropCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ i32
                    mutable pointer rawstring

        inline setScrollCallback (window callback)
            glfw.setScrollCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ f64 f64

        inline setJoystickCallback (window callback)
            glfw.setJoystickCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ i32 i32

        locals;

    glfw .. callbacks
