.RECIPEPREFIX+=*

SCOPES_INSTALL_DIRECTORY=/usr/local/lib/scopes

.PHONY: check install

check: init.sc
* scopes init.sc

install:
* rm -f $(SCOPES_INSTALL_DIRECTORY)/glfw
* ln -s $(shell pwd) $(SCOPES_INSTALL_DIRECTORY)/glfw

